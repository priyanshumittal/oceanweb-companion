=== Oceanweb Companion ===

Contributors: oceanwebthemes
Tags: widget, admin, widgets
Requires at least: 4.5
Tested up to: 5.7
Stable tag: 0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Enhances all WordPress Themes functionalities released by OceanwebThemes.

== Description ==

Oceanweb Companion is a plugin build to enhance the functionality of WordPress Theme made by OceanwebThemes. It creates the Homepage sections for the Oceanwebthemes like Slider section, Service section, Blog section, etc. It also creates repeater controls in the customizer settings allowing you to create a live site without moving out to the customizer screen. Right now plugin has support for BusiCare WordPress Theme. In the future, this plugin will support other themes by OceanwebThemes.


== Changelog ==

= 0.1 =
* Initial release.

== External resources ==

Alpha color picker Control
Copyright: (c) 2016 Codeinwp cristian-ungureanu
License: MIT License
Source: https://github.com/Codeinwp/customizer-controls/tree/master/customizer-alpha-color-picker

Repeater Control
Copyright: (c) 2016 Codeinwp cristian-ungureanu
License: MIT license
Source: https://github.com/Codeinwp/customizer-controls/tree/master/customizer-repeater

Custom control - Image Radio Button Custom Control
Copyright: Anthony Hortin
License: GNU General Public License v2 or later
Source: https://github.com/maddisondesigns/customizer-custom-controls

== Screenshots ==

* Image used in slider, License CC0 Public Domain
https://stocksnap.io/photo/woman-working-8LHTBAZW32

* Image used in testimonial, License CC0 Public Domain
1. https://pxhere.com/en/photo/1419926
2. https://pxhere.com/en/photo/642874
