#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: "
"Oceanweb-Companion\n"
"POT-Creation-Date: "
"2021-06-03 15:38+0530\n"
"PO-Revision-Date: \n"
"Last-Translator: Your "
"Name <you@example.com>\n"
"Language-Team: Your Team "
"<translations@example."
"com>\n"
"Report-Msgid-Bugs-To: "
"Translator Name "
"<translations@example."
"com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/"
"plain; charset=UTF-8\n"
"Content-Transfer-"
"Encoding: 8bit\n"
"Plural-Forms: "
"nplurals=2; plural=n != "
"1;\n"
"X-Textdomain-Support: "
"yesX-Generator: Poedit "
"1.6.4\n"
"X-Poedit-SourceCharset: "
"UTF-8\n"
"X-Poedit-KeywordsList: "
"__;_e;esc_html_e;"
"esc_html_x:1,2c;"
"esc_html__;esc_attr_e;"
"esc_attr_x:1,2c;"
"esc_attr__;_ex:1,2c;"
"_nx:4c,1,2;"
"_nx_noop:4c,1,2;_x:1,2c;"
"_n:1,2;_n_noop:1,2;"
"__ngettext:1,2;"
"__ngettext_noop:1,2;_c,"
"_nc:4c,1,2\n"
"X-Poedit-Basepath: ..\n"
"Language: en_US\n"
"X-Generator: Poedit 2.3\n"
"X-Poedit-"
"SearchPath-0: .\n"

#: inc/busicare/customizer.php:12
#: inc/busicare/sections/busicare-services-section.php:11
msgid ""
"Suspendisse Tristique"
msgstr ""

#: inc/busicare/customizer.php:13
#: inc/busicare/customizer.php:22
#: inc/busicare/customizer.php:31
#: inc/busicare/sections/busicare-services-section.php:12
#: inc/busicare/sections/busicare-services-section.php:21
#: inc/busicare/sections/busicare-services-section.php:30
msgid ""
"Sed ut perspiciatis unde "
"omnis iste natus error "
"sit voluptatem "
"accusantium doloremque "
"laudantium, totam rem "
"aperiam."
msgstr ""

#: inc/busicare/customizer.php:21
#: inc/busicare/sections/busicare-services-section.php:20
msgid "Blandit-Gravida"
msgstr ""

#: inc/busicare/customizer.php:30
#: inc/busicare/sections/busicare-services-section.php:29
msgid "Justo Bibendum"
msgstr ""

#: inc/busicare/customizer/cta1-section.php:5
msgid ""
"Callout 1 section "
"settings"
msgstr ""

#: inc/busicare/customizer/cta1-section.php:18
msgid ""
"Enable / Disable Home "
"Callout 1 section"
msgstr ""

#: inc/busicare/customizer/cta1-section.php:27
#: inc/busicare/sections/busicare-cta1-section.php:7
msgid ""
"Lorem ipsum dolor sit "
"amet?"
msgstr ""

#: inc/busicare/customizer/cta1-section.php:33
msgid "Tag line"
msgstr ""

#: inc/busicare/customizer/cta1-section.php:42
#: inc/busicare/sections/busicare-cta1-section.php:8
msgid "Nemo enim"
msgstr ""

#: inc/busicare/customizer/cta1-section.php:52
#: inc/busicare/customizer/slider-section.php:119
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:277
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:425
msgid "Button Text"
msgstr ""

#: inc/busicare/customizer/cta1-section.php:71
#: inc/busicare/customizer/slider-section.php:131
msgid "Button Link"
msgstr ""

#: inc/busicare/customizer/cta1-section.php:86
#: inc/busicare/customizer/news-section.php:131
#: inc/busicare/customizer/slider-section.php:145
#: inc/busicare/customizer/slider-section.php:183
#: inc/busicare/customizer/testimonial-section.php:164
msgid ""
"Open link in new tab"
msgstr ""

#: inc/busicare/customizer/news-section.php:4
msgid ""
"Latest News settings"
msgstr ""

#: inc/busicare/customizer/news-section.php:18
msgid ""
"Enable / Disable Home "
"News section"
msgstr ""

#: inc/busicare/customizer/news-section.php:28
msgid "Our Latest News"
msgstr ""

#: inc/busicare/customizer/news-section.php:33
#: inc/busicare/customizer/services-section.php:31
#: inc/busicare/customizer/slider-section.php:94
#: inc/busicare/customizer/testimonial-section.php:29
#: inc/busicare/customizer/testimonial-section.php:103
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:254
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:403
msgid "Title"
msgstr ""

#: inc/busicare/customizer/news-section.php:41
msgid "From our blog"
msgstr ""

#: inc/busicare/customizer/news-section.php:46
#: inc/busicare/customizer/services-section.php:47
msgid "Sub title"
msgstr ""

#: inc/busicare/customizer/news-section.php:55
#: inc/busicare/sections/busicare-news-section.php:94
msgid "Read More"
msgstr ""

#: inc/busicare/customizer/news-section.php:60
msgid "Read More Text"
msgstr ""

#: inc/busicare/customizer/news-section.php:77
msgid ""
"Enable / Disable post "
"meta in blog section"
msgstr ""

#: inc/busicare/customizer/news-section.php:87
msgid "View More"
msgstr ""

#: inc/busicare/customizer/news-section.php:96
msgid ""
"View More Button Text"
msgstr ""

#: inc/busicare/customizer/news-section.php:114
msgid ""
"View More Button Link"
msgstr ""

#: inc/busicare/customizer/services-section.php:3
msgid "Services settings"
msgstr ""

#: inc/busicare/customizer/services-section.php:16
msgid ""
"Enable / Disable "
"Services on homepage"
msgstr ""

#: inc/busicare/customizer/services-section.php:25
#: inc/busicare/sections/busicare-services-section.php:38
msgid "Etiam et Urna?"
msgstr ""

#: inc/busicare/customizer/services-section.php:40
#: inc/busicare/sections/busicare-services-section.php:39
msgid "Fusce Sed Massa"
msgstr ""

#: inc/busicare/customizer/services-section.php:59
msgid "Services content"
msgstr ""

#: inc/busicare/customizer/services-section.php:62
msgid "Add new Service"
msgstr ""

#: inc/busicare/customizer/services-section.php:63
msgid "Service"
msgstr ""

#: inc/busicare/customizer/services-section.php:77
msgid ""
"To add More Service? Then"
msgstr ""

#: inc/busicare/customizer/services-section.php:78
msgid "Upgrade to Plus"
msgstr ""

#: inc/busicare/customizer/slider-section.php:5
msgid "Slider settings"
msgstr ""

#: inc/busicare/customizer/slider-section.php:18
msgid ""
"Enable / Disable Slider "
"on homepage"
msgstr ""

#: inc/busicare/customizer/slider-section.php:36
#: inc/busicare/customizer/testimonial-section.php:87
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:555
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:601
msgid "Image"
msgstr ""

#: inc/busicare/customizer/slider-section.php:52
msgid ""
"Enable / Disable slider "
"image overlay"
msgstr ""

#: inc/busicare/customizer/slider-section.php:67
msgid ""
"Slider image overlay "
"color"
msgstr ""

#: inc/busicare/customizer/slider-section.php:76
msgid "Nulla nec dolor sit"
msgstr ""

#: inc/busicare/customizer/slider-section.php:81
msgid "Sub-title"
msgstr ""

#: inc/busicare/customizer/slider-section.php:89
#: inc/busicare/sections/busicare-slider-section.php:9
#: inc/busicare/sections/busicare-slider-section.php:11
msgid ""
"Nulla nec dolor sit amet "
"lacus molestie"
msgstr ""

#: inc/busicare/customizer/slider-section.php:102
msgid ""
"Sea summo mazim ex, ea "
"errem eleifend "
"definitionem vim. Ut nec "
"hinc dolor possim <br> "
"mei ludus efficiendi ei "
"sea summo mazim ex."
msgstr ""

#: inc/busicare/customizer/slider-section.php:106
#: inc/busicare/customizer/testimonial-section.php:115
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:268
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:417
msgid "Description"
msgstr ""

#: inc/busicare/customizer/slider-section.php:115
#: inc/busicare/sections/busicare-slider-section.php:13
msgid "Nec Sem"
msgstr ""

#: inc/busicare/customizer/slider-section.php:127
#: inc/busicare/customizer/slider-section.php:165
#: inc/busicare/customizer/testimonial-section.php:148
#: inc/busicare/sections/busicare-news-section.php:7
#: inc/busicare/sections/busicare-testimonial-section.php:10
msgid "#"
msgstr ""

#: inc/busicare/customizer/slider-section.php:153
#: inc/busicare/customizer/testimonial-section.php:124
#: inc/busicare/sections/busicare-news-section.php:8
#: inc/busicare/sections/busicare-slider-section.php:17
#: inc/busicare/sections/busicare-testimonial-section.php:8
msgid "Cras Vitae"
msgstr ""

#: inc/busicare/customizer/slider-section.php:157
msgid "Button 2 Text"
msgstr ""

#: inc/busicare/customizer/slider-section.php:169
msgid "Button 2 Link"
msgstr ""

#: inc/busicare/customizer/testimonial-section.php:4
msgid ""
"Testimonials settings"
msgstr ""

#: inc/busicare/customizer/testimonial-section.php:17
msgid ""
"Enable / Disable "
"Testimonial on homepage"
msgstr ""

#: inc/busicare/customizer/testimonial-section.php:25
msgid "Proin Egestas"
msgstr ""

#: inc/busicare/customizer/testimonial-section.php:42
msgid "Background Image"
msgstr ""

#: inc/busicare/customizer/testimonial-section.php:55
msgid ""
"Enable / Disable "
"testimonial image overlay"
msgstr ""

#: inc/busicare/customizer/testimonial-section.php:68
msgid ""
"Testimonial image "
"overlay color"
msgstr ""

#: inc/busicare/customizer/testimonial-section.php:99
#: inc/busicare/sections/busicare-testimonial-section.php:7
msgid ""
"Duis aute irure dolor in "
"reprehenderit"
msgstr ""

#: inc/busicare/customizer/testimonial-section.php:111
#: inc/busicare/sections/busicare-testimonial-section.php:13
msgid ""
"Sed ut Perspiciatis Unde "
"Omnis Iste Sed ut "
"perspiciatis unde omnis "
"iste natu error sit "
"voluptatem accu tium "
"neque fermentum veposu "
"miten a tempor nise. "
"Duis autem vel eum "
"iriure dolor in "
"hendrerit in vulputate "
"velit consequat "
"reprehender in voluptate "
"velit esse cillum duis "
"dolor fugiat nulla "
"pariatur."
msgstr ""

#: inc/busicare/customizer/testimonial-section.php:128
msgid "Client Name"
msgstr ""

#: inc/busicare/customizer/testimonial-section.php:136
#: inc/busicare/sections/busicare-testimonial-section.php:9
msgid "Eu Suscipit"
msgstr ""

#: inc/busicare/customizer/testimonial-section.php:140
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:349
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:473
msgid "Designation"
msgstr ""

#: inc/busicare/customizer/testimonial-section.php:152
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:288
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:435
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:624
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:646
msgid "Link"
msgstr ""

#: inc/busicare/sections/busicare-news-section.php:18
msgid "Vitae Lacinia"
msgstr ""

#: inc/busicare/sections/busicare-news-section.php:19
msgid "Cras Vitae Placerat"
msgstr ""

#: inc/busicare/sections/busicare-news-section.php:71
msgid "By"
msgstr ""

#: inc/busicare/sections/busicare-slider-section.php:12
msgid ""
"Sea summo mazim ex, ea "
"errem eleifend "
"definitionem vim. Ut nec "
"hinc dolor possim <br> "
"mei ludus  efficiendi ei "
"sea summo mazim ex."
msgstr ""

#: inc/busicare/sections/busicare-testimonial-section.php:17
msgid ""
"Nam Viverra Iaculis "
"Finibus"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:33
msgid "Add new field"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:38
msgid "Customizer Repeater"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:261
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:410
msgid "Subtitle"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:307
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:453
msgid "Video Url"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:331
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:395
msgid "Color"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:341
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:464
msgid "Shortcode"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:365
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:484
msgid "Delete field"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:521
msgid ""
"Open link in new tab:"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:532
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:600
msgid "Icon"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:537
#, php-format
msgid ""
"Note: Some icons may not "
"be displayed here. You "
"can see the full list of "
"icons at %1$s."
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:558
msgid "Upload Image"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:567
msgid "Slide Format"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:571
msgid "Standard"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:575
msgid "Aside"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:579
msgid "Quote"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:583
msgid "Status"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:587
msgid "Video"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:597
msgid "Image type"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:602
msgid "None"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:610
msgid "Social icons"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:627
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:657
msgid "Remove Icon"
msgstr ""

#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:632
#: inc/controls/customizer-repeater/class/customizer-repeater-control.php:665
msgid "Add Icon"
msgstr ""

#: inc/controls/customizer-repeater/inc/icons.php:1
msgid "Type to filter"
msgstr ""
