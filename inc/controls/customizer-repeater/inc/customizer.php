<?php
function oceanweb_companion_repeater_register( $wp_customize ) {

	$Oceanweb_Companion_Repeater_path = OCEANWEB_COMPANION_PLUGIN_DIR . '/inc/controls/customizer-repeater/class/customizer-repeater-control.php';
	if( file_exists( $Oceanweb_Companion_Repeater_path ) ){
		require_once( $Oceanweb_Companion_Repeater_path );
	}

}
add_action( 'customize_register', 'oceanweb_companion_repeater_register' );

function oceanweb_companion_repeater_sanitize($input){
	$busicare_input_decoded = json_decode($input,true);

	if(!empty($busicare_input_decoded)) {
		foreach ($busicare_input_decoded as $boxk => $box ){
			foreach ($box as $key => $value){

					$busicare_input_decoded[$boxk][$key] = wp_kses_post( force_balance_tags( $value ) );

			}
		}
		return json_encode($busicare_input_decoded);
	}
	return $input;
}