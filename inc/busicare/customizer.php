<?php
// busicare default service data
if (!function_exists('oceanweb_companion_busicare_service_default_customize_register')) :

    function oceanweb_companion_busicare_service_default_customize_register($wp_customize) {

        $busicare_service_content_control = $wp_customize->get_setting('busicare_service_content');
        if (!empty($busicare_service_content_control)) {
            $busicare_service_content_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-headphones',
                    'title' => esc_html__('Suspendisse Tristique', 'oceanweb-companion'),
                    'text' => esc_html__('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.', 'oceanweb-companion'),
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b56',
                ),
                array(
                    'icon_value' => 'fa-mobile',
                    'title' => esc_html__('Blandit-Gravida', 'oceanweb-companion'),
                    'text' => esc_html__('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.', 'oceanweb-companion'),
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b66',
                ),
                array(
                    'icon_value' => 'fa fa-cogs',
                    'title' => esc_html__('Justo Bibendum', 'oceanweb-companion'),
                    'text' => esc_html__('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.', 'oceanweb-companion'),
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b86',
                ),
            ));
        }
    }

    add_action('customize_register', 'oceanweb_companion_busicare_service_default_customize_register');

endif;
