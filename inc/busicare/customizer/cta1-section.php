<?php

//Callout Section
$wp_customize->add_section('home_cta1_page_section', array(
    'title' => esc_html__('Callout 1 section settings', 'oceanweb-companion'),
    'panel' => 'section_settings',
    'priority' => 10,
));

// Enable call to action section
$wp_customize->add_setting('cta1_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'oceanweb_companion_busicare_sanitize_checkbox',
    ));

$wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'cta1_section_enable',
                array(
            'label' => esc_html__('Enable / Disable Home Callout 1 section', 'oceanweb-companion'),
            'type' => 'toggle',
            'section' => 'home_cta1_page_section',
                )
));

$wp_customize->add_setting(
        'home_cta1_title',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet?', 'oceanweb-companion'),
            'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
            'transport' => $selective_refresh,
        )
);
$wp_customize->add_control('home_cta1_title', array(
    'label' => esc_html__('Tag line', 'oceanweb-companion'),
    'section' => 'home_cta1_page_section',
    'type' => 'text',
    'active_callback' => 'busicare_cta1_callback',
    ));

$wp_customize->add_setting(
        'home_cta1_btn_text',
        array(
            'default' => esc_html__('Nemo enim', 'oceanweb-companion'),
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
            'transport' => $selective_refresh,
        )
);

$wp_customize->add_control(
        'home_cta1_btn_text',
        array(
            'label' => esc_html__('Button Text', 'oceanweb-companion'),
            'section' => 'home_cta1_page_section',
            'type' => 'text',
            'active_callback' => 'busicare_cta1_callback'
));

$wp_customize->add_setting(
        'home_cta1_btn_link',
        array(
            'default' => '#',
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'esc_url_raw',
            'transport' => $selective_refresh,
));


$wp_customize->add_control(
        'home_cta1_btn_link',
        array(
            'label' => esc_html__('Button Link', 'oceanweb-companion'),
            'section' => 'home_cta1_page_section',
            'type' => 'text',
            'active_callback' => 'busicare_cta1_callback'
));

$wp_customize->add_setting(
        'home_cta1_btn_link_target',
        array('sanitize_callback' => 'oceanweb_companion_busicare_sanitize_checkbox',
));

$wp_customize->add_control(
        'home_cta1_btn_link_target',
        array(
            'type' => 'checkbox',
            'label' => esc_html__('Open link in new tab', 'oceanweb-companion'),
            'section' => 'home_cta1_page_section',
            'active_callback' => 'busicare_cta1_callback'
        )
);


/**
 * Add selective refresh for Front page pricing section controls.
 */
$wp_customize->selective_refresh->add_partial('home_cta1_title', array(
    'selector' => '.cta_main .cta_content h1',
    'settings' => 'home_cta1_title',
    'render_callback' => 'oceanweb_companion_home_cta1_title_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_cta1_btn_text', array(
    'selector' => '.cta_main .cta_content a',
    'settings' => 'home_cta1_btn_text',
    'render_callback' => 'oceanweb_companion_home_cta1_btn_text_render_callback',
));
function oceanweb_companion_home_cta1_title_render_callback() {
    return get_theme_mod('home_cta1_title');
}
function oceanweb_companion_home_cta1_btn_text_render_callback() {
    return get_theme_mod('home_cta1_btn_text');
}
?>