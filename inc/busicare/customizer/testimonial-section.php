<?php
	/* Testimonial Section */
	$wp_customize->add_section('testimonial_section', array(
	    'title' => esc_html__('Testimonials settings', 'oceanweb-companion'),
	    'panel' => 'section_settings',
	    'priority' => 7,
	));

	// Enable testimonial section
	$wp_customize->add_setting('testimonial_section_enable', array(
	    'default' => true,
	    'sanitize_callback' => 'oceanweb_companion_busicare_sanitize_checkbox'
	    ));

	$wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'testimonial_section_enable',
	                array(
	            'label' => esc_html__('Enable / Disable Testimonial on homepage', 'oceanweb-companion'),
	            'type' => 'toggle',
	            'section' => 'testimonial_section',
	                )
	));
	// testimonial section title
	$wp_customize->add_setting('home_testimonial_section_title', array(
	    'capability' => 'edit_theme_options',
	    'default' => esc_html__('Proin Egestas', 'oceanweb-companion'),
	    'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
	));
	$wp_customize->add_control('home_testimonial_section_title', array(
	    'label' => esc_html__('Title', 'oceanweb-companion'),
	    'section' => 'testimonial_section',
	    'type' => 'text',
	    'active_callback' => 'busicare_testimonial_callback'
	));

	//Testimonial Background Image
	$wp_customize->add_setting('testimonial_callout_background', array(
		'default' => OCEANWEB_COMPANION_PLUGIN_URL . '/inc/busicare/images/testimonial/testimonial-bg.jpg',
	    'sanitize_callback' => 'esc_url_raw',
	));

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'testimonial_callout_background', array(
	            'label' => esc_html__('Background Image', 'oceanweb-companion'),
	            'section' => 'testimonial_section',
	            'settings' => 'testimonial_callout_background',
	            'active_callback' => 'busicare_testimonial_callback'
	        )));

	// Image overlay
	$wp_customize->add_setting('testimonial_image_overlay', array(
	    'default' => true,
	    'sanitize_callback' => 'oceanweb_companion_busicare_sanitize_checkbox',
	));

	$wp_customize->add_control('testimonial_image_overlay', array(
	    'label' => esc_html__('Enable / Disable testimonial image overlay', 'oceanweb-companion'),
	    'section' => 'testimonial_section',
	    'type' => 'checkbox',
	    'active_callback' => 'busicare_testimonial_callback'
	));

	//Testimonial Background Overlay Color
	$wp_customize->add_setting('testimonial_overlay_section_color', array(
	    'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
	    'default' => 'rgba(0, 11, 24, 0.8)',
	));

	$wp_customize->add_control(new Oceanweb_Companion_Customize_Alpha_Color_Control($wp_customize, 'testimonial_overlay_section_color', array(
	            'label' => esc_html__('Testimonial image overlay color', 'oceanweb-companion'),
	            'palette' => true,
	            'section' => 'testimonial_section',
	            'active_callback' => 'busicare_testimonial_callback'
	        )
	));

	//testimonial one image
	$wp_customize->add_setting( 'home_testimonial_thumb',
    		array(
    			'default' => OCEANWEB_COMPANION_PLUGIN_URL .'/inc/busicare/images/testimonial/testi1.jpg',
				'sanitize_callback' => 'esc_url_raw',
			));
	
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'home_testimonial_thumb',
			array(
				'label' => esc_html__('Image','oceanweb-companion'),
				'settings' =>'home_testimonial_thumb',
				'section' => 'testimonial_section',
				'type' => 'upload',
				'active_callback' => 'busicare_testimonial_callback'
			)
		)
	);
	
	// testimonial section title
	$wp_customize->add_setting( 'home_testimonial_title',array(
	'capability'     => 'edit_theme_options',
	'default' => esc_html__('Duis aute irure dolor in reprehenderit','oceanweb-companion'),
	'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
	));	
	$wp_customize->add_control( 'home_testimonial_title',array(
	'label'   => esc_html__('Title','oceanweb-companion'),
	'section' => 'testimonial_section',
	'type' => 'text',
	'active_callback' => 'busicare_testimonial_callback'
	));	
	//testimonial description
	$wp_customize->add_setting( 'home_testimonial_desc',array(
	'capability'     => 'edit_theme_options',
	'default' => esc_html__('Sed ut Perspiciatis Unde Omnis Iste Sed ut perspiciatis unde omnis iste natu error sit voluptatem accu tium neque fermentum veposu miten a tempor nise. Duis autem vel eum iriure dolor in hendrerit in vulputate velit consequat reprehender in voluptate velit esse cillum duis dolor fugiat nulla pariatur.','oceanweb-companion'),
	'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
	));	
	$wp_customize->add_control( 'home_testimonial_desc',array(
	'label'   => esc_html__('Description','oceanweb-companion'),
	'section' => 'testimonial_section',
	'type' => 'text',
	'active_callback' => 'busicare_testimonial_callback'
	));
		
	// testimonial section name
	$wp_customize->add_setting( 'home_testimonial_name',array(
	'capability'     => 'edit_theme_options',
	'default' => esc_html__('Cras Vitae','oceanweb-companion'),
	'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
	));	
	$wp_customize->add_control( 'home_testimonial_name',array(
	'label'   => esc_html__('Client Name','oceanweb-companion'),
	'section' => 'testimonial_section',
	'type' => 'text',
	'active_callback' => 'busicare_testimonial_callback'
	));
			
	$wp_customize->add_setting( 'home_testimonial_designation',array(
	'capability'     => 'edit_theme_options',
	'default' => esc_html__('Eu Suscipit','oceanweb-companion'),
	'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
	));	
	$wp_customize->add_control( 'home_testimonial_designation',array(
	'label'   => esc_html__('Designation','oceanweb-companion'),
	'section' => 'testimonial_section',
	'type' => 'text',
	'active_callback' => 'busicare_testimonial_callback'
	));

	// Testimonial User link
	$wp_customize->add_setting('home_testimonial_link', array(
	    'default' => esc_html__('#', 'oceanweb-companion'),
	    'sanitize_callback' => 'esc_url_raw',
	));
	$wp_customize->add_control('home_testimonial_link', array(
	    'label' => esc_html__('Link', 'oceanweb-companion'),
	    'section' => 'testimonial_section',
	    'type' => 'text',
	    'active_callback' => 'busicare_testimonial_callback'
	));

	// testimonial open in new tab
	$wp_customize->add_setting( 'home_testimonial_open_tab',array(
	'default' => false,
	'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
	));	
	$wp_customize->add_control( 'home_testimonial_open_tab',array(
	'label'   => esc_html__('Open link in new tab','spicebox'),
	'section' => 'testimonial_section',
	'type' => 'checkbox',
	'active_callback' => 'busicare_testimonial_callback'
	));	

$wp_customize->selective_refresh->add_partial('home_testimonial_section_title', array(
    'selector' => '.testimonial h2',
    'settings' => 'home_testimonial_section_title',
    'render_callback' => 'home_testimonial_section_title_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_testimonial_thumb', array(
    'selector' => '.testimonial .testmonial-block img',
    'settings' => 'home_testimonial_thumb',
    'render_callback' => 'home_testimonial_section_title_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_testimonial_title', array(
    'selector' => '.testimonial .testmonial-block .entry-content .title span',
    'settings' => 'home_testimonial_title',
    'render_callback' => 'home_testimonial_section_title_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_testimonial_desc', array(
    'selector' => '.testimonial .testmonial-block .entry-content p',
    'settings' => 'home_testimonial_desc',
    'render_callback' => 'home_testimonial_section_title_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_testimonial_name', array(
    'selector' => '.testimonial .testmonial-block figcaption .name',
    'settings' => 'home_testimonial_name',
    'render_callback' => 'home_testimonial_section_title_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_testimonial_designation', array(
    'selector' => '.testimonial .testmonial-block figcaption .designation ',
    'settings' => 'home_testimonial_designation',
    'render_callback' => 'home_testimonial_section_title_render_callback',
));