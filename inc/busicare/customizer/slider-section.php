<?php

	/* Slider Section */
	$wp_customize->add_section('slider_section', array(
	    'title' => esc_html__('Slider settings', 'oceanweb-companion'),
	    'panel' => 'section_settings',
	    'priority' => 1,
	));

	// Enable slider
	$wp_customize->add_setting('home_page_slider_enabled', array(
	    'default' => true,
	    'sanitize_callback' => 'oceanweb_companion_busicare_sanitize_checkbox',
	));

	$wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'home_page_slider_enabled',
	                array(
	            'label' => esc_html__('Enable / Disable Slider on homepage', 'oceanweb-companion'),
	            'type' => 'toggle',
	            'section' => 'slider_section',
	            'priority' => 1,
	                )
	));

	$wp_customize->add_setting('home_slider_image', array(
		'default' => OCEANWEB_COMPANION_PLUGIN_URL . '/inc/busicare/images/slider/slider.jpg',
	    'sanitize_callback' => 'esc_url_raw',
	));

	$wp_customize->add_control(
	    new WP_Customize_Image_Control(
	            $wp_customize,
	            'home_slider_image',
	            array(
	        'type' => 'upload',
	        'label' => esc_html__('Image', 'oceanweb-companion'),
	        'settings' => 'home_slider_image',
	        'section' => 'slider_section',
	        'active_callback' => 'busicare_slider_callback'
	            )
	    )
	);

	// Image overlay
	$wp_customize->add_setting('slider_image_overlay', array(
	    'default' => true,
	    'sanitize_callback' => 'oceanweb_companion_busicare_sanitize_checkbox',
	        )
	);

	$wp_customize->add_control('slider_image_overlay', array(
	    'label' => esc_html__('Enable / Disable slider image overlay', 'oceanweb-companion'),
	    'section' => 'slider_section',
	    'type' => 'checkbox',
	    'active_callback' => 'busicare_slider_callback'
	        )
	);

	//Slider Background Overlay Color
	$wp_customize->add_setting('slider_overlay_section_color', array(
	    'sanitize_callback' => 'sanitize_text_field',
	    'default' => 'rgba(0,0,0,0.6)',
	        )
	);

	$wp_customize->add_control(new Oceanweb_Companion_Customize_Alpha_Color_Control($wp_customize, 'slider_overlay_section_color', array(
	            'label' => esc_html__('Slider image overlay color', 'oceanweb-companion'),
	            'palette' => true,
	            'section' => 'slider_section',
	            'active_callback' => 'busicare_slider_callback'
	                )
	));

	// Slider subtitle
	$wp_customize->add_setting('home_slider_subtitle', array(
	    'default' => esc_html__('Nulla nec dolor sit', 'oceanweb-companion'),
	    'capability' => 'edit_theme_options',
	    'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
	));
	$wp_customize->add_control('home_slider_subtitle', array(
	    'label' => esc_html__('Sub-title', 'oceanweb-companion'),
	    'section' => 'slider_section',
	    'type' => 'text',
	    'active_callback' => 'busicare_slider_callback'
	)); 

	// Slider title
	$wp_customize->add_setting('home_slider_title', array(
	    'default' => esc_html__('Nulla nec dolor sit amet lacus molestie', 'oceanweb-companion'),
	    'capability' => 'edit_theme_options',
	    'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
	));
	$wp_customize->add_control('home_slider_title', array(
	    'label' => esc_html__('Title', 'oceanweb-companion'),
	    'section' => 'slider_section',
	    'type' => 'text',
	    'active_callback' => 'busicare_slider_callback'
	));

	//Slider discription
	$wp_customize->add_setting('home_slider_discription', array(
	    'default' => esc_html__('Sea summo mazim ex, ea errem eleifend definitionem vim. Ut nec hinc dolor possim <br> mei ludus efficiendi ei sea summo mazim ex.', 'oceanweb-companion'),
	    'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
	));
	$wp_customize->add_control('home_slider_discription', array(
	    'label' => esc_html__('Description', 'oceanweb-companion'),
	    'section' => 'slider_section',
	    'type' => 'textarea',
	    'active_callback' => 'busicare_slider_callback'
	));


	// Slider button text
	$wp_customize->add_setting('home_slider_btn_txt', array(
	    'default' => esc_html__('Nec Sem', 'oceanweb-companion'),
	    'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
	));
	$wp_customize->add_control('home_slider_btn_txt', array(
	    'label' => esc_html__('Button Text', 'oceanweb-companion'),
	    'section' => 'slider_section',
	    'type' => 'text',
	    'active_callback' => 'busicare_slider_callback'
	));

	// Slider button link
	$wp_customize->add_setting('home_slider_btn_link', array(
	    'default' => esc_html__('#', 'oceanweb-companion'),
	    'sanitize_callback' => 'esc_url_raw',
	));
	$wp_customize->add_control('home_slider_btn_link', array(
	    'label' => esc_html__('Button Link', 'oceanweb-companion'),
	    'section' => 'slider_section',
	    'type' => 'text',
	    'active_callback' => 'busicare_slider_callback'
	));

	// Slider button target
	$wp_customize->add_setting(
	        'home_slider_btn_target',
	        array(
	            'default' => false,
	            'sanitize_callback' => 'oceanweb_companion_busicare_sanitize_checkbox',
	));
	$wp_customize->add_control('home_slider_btn_target', array(
	    'label' => esc_html__('Open link in new tab', 'oceanweb-companion'),
	    'section' => 'slider_section',
	    'type' => 'checkbox',
	    'active_callback' => 'busicare_slider_callback'
	));

	// Slider button2 text
	$wp_customize->add_setting('home_slider_btn_txt2', array(
	    'default' => esc_html__('Cras Vitae', 'oceanweb-companion'),
	    'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
	));
	$wp_customize->add_control('home_slider_btn_txt2', array(
	    'label' => esc_html__('Button 2 Text', 'oceanweb-companion'),
	    'section' => 'slider_section',
	    'type' => 'text',
	    'active_callback' => 'busicare_slider_callback'
	));

	// Slider button link
	$wp_customize->add_setting('home_slider_btn_link2', array(
	    'default' => esc_html__('#', 'oceanweb-companion'),
	    'sanitize_callback' => 'esc_url_raw',
	));
	$wp_customize->add_control('home_slider_btn_link2', array(
	    'label' => esc_html__('Button 2 Link', 'oceanweb-companion'),
	    'section' => 'slider_section',
	    'type' => 'text',
	    'active_callback' => 'busicare_slider_callback'
	));

	// Slider button target
	$wp_customize->add_setting(
	        'home_slider_btn_target2',
	        array(
	            'default' => false,
	            'sanitize_callback' => 'oceanweb_companion_busicare_sanitize_checkbox',
	));
	$wp_customize->add_control('home_slider_btn_target2', array(
	    'label' => esc_html__('Open link in new tab', 'oceanweb-companion'),
	    'section' => 'slider_section',
	    'type' => 'checkbox',
	    'active_callback' => 'busicare_slider_callback'
	));


$wp_customize->selective_refresh->add_partial('home_slider_subtitle', array(
    'selector' => '.bcslider-section .slider-caption .heading ',
    'settings' => 'home_slider_subtitle',
    'render_callback' => 'home_slider_section_title_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_slider_title', array(
    'selector' => '.bcslider-section .slider-caption .title ',
    'settings' => 'home_slider_title',
    'render_callback' => 'home_slider_section_title_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_slider_discription', array(
    'selector' => '.bcslider-section .slider-caption .description ',
    'settings' => 'home_slider_discription',
    'render_callback' => 'home_slider_section_title_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_slider_btn_txt', array(
    'selector' => '.bcslider-section .slider-caption .btn-combo .btn-default ',
    'settings' => 'home_slider_btn_txt',
    'render_callback' => 'home_slider_section_title_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_slider_btn_txt2', array(
    'selector' => '.bcslider-section .slider-caption .btn-combo .btn-light ',
    'settings' => 'home_slider_btn_txt2',
    'render_callback' => 'home_slider_section_title_render_callback',
));