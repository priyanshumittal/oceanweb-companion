<?php
$wp_customize->add_section('services_section', array(
    'title' => esc_html__('Services settings', 'oceanweb-companion'),
    'panel' => 'section_settings',
    'priority' => 2,
));

// Enable service more btn
$wp_customize->add_setting('home_service_section_enabled', array(
    'default' => true,
    'sanitize_callback' => 'oceanweb_companion_busicare_sanitize_checkbox'
    ));

$wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'home_service_section_enabled',
                array(
            'label' => esc_html__('Enable / Disable Services on homepage', 'oceanweb-companion'),
            'type' => 'toggle',
            'section' => 'services_section',
                )
));

//Service section title
$wp_customize->add_setting('home_service_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => esc_html__('Etiam et Urna?', 'oceanweb-companion'),
    'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
    'transport' => $selective_refresh,
));

$wp_customize->add_control('home_service_section_title', array(
    'label' => esc_html__('Title', 'oceanweb-companion'),
    'section' => 'services_section',
    'type' => 'text',
    'active_callback' => 'busicare_service_callback'
));

// Service section description
$wp_customize->add_setting('home_service_section_discription', array(
    'capability' => 'edit_theme_options',
    'default' => esc_html__('Fusce Sed Massa', 'oceanweb-companion'),
    'sanitize_callback' => 'oceanweb_companion_busicare_home_page_sanitize_text',
    'transport' => $selective_refresh,
));


$wp_customize->add_control('home_service_section_discription', array(
    'label' => esc_html__('Sub title', 'oceanweb-companion'),
    'section' => 'services_section',
    'type' => 'text',
    'active_callback' => 'busicare_service_callback'
));



if (class_exists('Oceanweb_Companion_Repeater')) {
    $wp_customize->add_setting('busicare_service_content', array());

    $wp_customize->add_control(new Oceanweb_Companion_Repeater($wp_customize, 'busicare_service_content', array(
                'label' => esc_html__('Services content', 'oceanweb-companion'),
                'section' => 'services_section',
                'priority' => 10,
                'add_field_label' => esc_html__('Add new Service', 'oceanweb-companion'),
                'item_name' => esc_html__('Service', 'oceanweb-companion'),
                'customizer_repeater_icon_control' => true,
                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'customizer_repeater_link_control' => true,
                'customizer_repeater_checkbox_control' => true,
                'customizer_repeater_image_control' => true,
                'active_callback' => 'busicare_service_callback'
    )));
}

class Busicare_services__section_upgrade extends WP_Customize_Control {
            public function render_content() { ?>
                <h3 class="customizer_busicareservice_upgrade_section" style="display: none;">
        <?php _e('To add More Service? Then','spicebox'); ?><a href="<?php echo esc_url( 'https://oceanwebthemes.com/busicare-pro' ); ?>" target="_blank">
                    <?php _e('Upgrade to Plus','spicebox'); ?> </a>  
                </h3>
            <?php
            }
        }
        
        $wp_customize->add_setting( 'busicare_service_upgrade_to_pro', array(
            'capability'            => 'edit_theme_options',
        ));
        $wp_customize->add_control(
            new Busicare_services__section_upgrade(
            $wp_customize,
            'busicare_service_upgrade_to_pro',
                array(
                    'section'               => 'services_section',
                    'settings'              => 'busicare_service_upgrade_to_pro',
                )
            )
        );

$wp_customize->selective_refresh->add_partial('home_service_section_title', array(
    'selector' => '.services .section-title, .services2 .section-title, .services3 .section-title, .services4 .section-title',
    'settings' => 'home_service_section_title',
    'render_callback' => 'oceanweb_companion_home_service_section_title_render_callback'
));

$wp_customize->selective_refresh->add_partial('home_service_section_discription', array(
    'selector' => '.services .section-subtitle, .services2 .section-subtitle, .services3 .section-subtitle, .services4 .section-subtitle',
    'settings' => 'home_service_section_discription',
    'render_callback' => 'oceanweb_companion_home_service_section_discription_render_callback'
));

$wp_customize->selective_refresh->add_partial('service_viewmore_btn_text', array(
    'selector' => '.services .view-more-services',
    'settings' => 'service_viewmore_btn_text',
    'render_callback' => 'oceanweb_companion_service_viewmore_btn_text_render_callback'
));

function oceanweb_companion_home_service_section_title_render_callback() {
    return get_theme_mod('home_service_section_title');
}

function oceanweb_companion_home_service_section_discription_render_callback() {
    return get_theme_mod('home_service_section_discription');
}

function oceanweb_companion_service_viewmore_btn_text_render_callback() {
    return get_theme_mod('service_viewmore_btn_text');
}