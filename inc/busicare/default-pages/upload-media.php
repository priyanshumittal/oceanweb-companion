<?php

$oceanweb_companion_ImagePath = OCEANWEB_COMPANION_PLUGIN_URL . 'inc/busicare/images';
    $oceanweb_companion_images = array(
        $oceanweb_companion_ImagePath . '/logo.png',
    );
foreach ($oceanweb_companion_images as $oceanweb_companion_name) {
    $oceanweb_companion_filename = basename($oceanweb_companion_name);
    $oceanweb_companion_upload_file = wp_upload_bits($oceanweb_companion_filename, null, file_get_contents($oceanweb_companion_name));
    if (!$oceanweb_companion_upload_file['error']) {
        $oceanweb_companion_wp_filetype = wp_check_filetype($oceanweb_companion_filename, null);
        $oceanweb_companion_attachment = array(
            'post_mime_type' => $oceanweb_companion_wp_filetype['type'],
            //'post_parent' => $parent_post_id,
            'post_title' => preg_replace('/\.[^.]+$/', '', $oceanweb_companion_filename),
            'post_status' => 'inherit'
        );
        $oceanweb_companion_ImageId[] = $oceanweb_companion_attachment_id = wp_insert_attachment($oceanweb_companion_attachment, $oceanweb_companion_upload_file['file']);

        if (!is_wp_error($oceanweb_companion_attachment_id)) {
            require_once(ABSPATH . "wp-admin" . '/includes/image.php');
            $oceanweb_companion_attachment_data = wp_generate_attachment_metadata($oceanweb_companion_attachment_id, $oceanweb_companion_upload_file['file']);
            wp_update_attachment_metadata($oceanweb_companion_attachment_id, $oceanweb_companion_attachment_data);
        }
    }
}
update_option('innofit_media_id', $oceanweb_companion_ImageId);
$oceanweb_companion_MediaId = get_option('innofit_media_id');
set_theme_mod('custom_logo', $oceanweb_companion_MediaId[0]);
set_theme_mod('header_textcolor', "blank");
?>