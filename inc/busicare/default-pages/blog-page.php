<?php
	$oceanweb_companion_post = array(
		  'comment_status' => 'closed',
		  'ping_status' =>  'closed' ,
		  'post_author' => 1,
		  'post_date' => date('Y-m-d H:i:s'),
		  'post_name' => 'Blog',
		  'post_status' => 'publish' ,
		  'post_title' => 'Blog',
		  'post_type' => 'page',
	);  
	//insert page and save the id
	$oceanweb_companion_newvalue = wp_insert_post( $oceanweb_companion_post, false );
	if ( $oceanweb_companion_newvalue && ! is_wp_error( $oceanweb_companion_newvalue ) ){
		update_post_meta( $oceanweb_companion_newvalue, '_wp_page_template', 'page.php' );
		
		// Use a static front page
		$oceanweb_companion_page = get_page_by_title('Blog');
		update_option( 'show_on_front', 'page' );
		update_option( 'page_for_posts', $oceanweb_companion_page->ID );
		
	}