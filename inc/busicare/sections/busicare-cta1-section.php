<?php
//call the action for the cta1 section
add_action('oceanweb_companion_busicare_cta1_action','oceanweb_companion_busicare_cta1_section');
//function for the cta1 section
function oceanweb_companion_busicare_cta1_section(){ 
	if(get_theme_mod('cta1_section_enable',true)==true) {
		$home_cta1_title = get_theme_mod('home_cta1_title', __('Lorem ipsum dolor sit amet?', 'oceanweb-companion'));
		$home_cta1_btn_text = get_theme_mod('home_cta1_btn_text', __('Nemo enim', 'oceanweb-companion'));
		$cta1_button_link = get_theme_mod('home_cta1_btn_link', '#');
		$home_cta1_btn_link_target = get_theme_mod('home_cta1_btn_link_target', false);
		$callout_cta1_background = get_theme_mod('callout_cta1_background', '');
		if ($callout_cta1_background != '') {
		    ?>
		    <section class="cta_main"  style="background-color: <?php echo esc_url($callout_cta1_background); ?>">
		        <?php
		}
		else {?>
        <section class="cta_main" >
        <?php }
        if (!empty($home_cta1_title) || (!empty($home_cta1_btn_text))): ?>
            <div class="container">
                <div class="row cta_content">
                    <div class="col-lg-8 col-md-6 col-sm-12">
                        <?php if (!empty($home_cta1_title)): ?><h1><?php echo esc_html($home_cta1_title); ?></h1><?php endif; ?>
                    </div>	
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?php if ($home_cta1_btn_text != '') {?>
                            <a class="btn-small btn-light cta_btn" <?php if ($cta1_button_link != '') { ?> href="<?php echo esc_url($cta1_button_link); ?>" <?php if($home_cta1_btn_link_target == true){ echo "target='_blank'";}}else{echo "href='#'";}?>><?php echo esc_html($home_cta1_btn_text); ?></a>

                        <?php } ?>
                    </div>
                </div>
            </div>
		<?php endif; ?>
		    </section>
		<?php	
		} 
}