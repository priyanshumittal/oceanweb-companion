<?php 
/* Call the action for team section */
add_action('oceanweb_companion_busicare_testimonial_action','oceanweb_companion_busicare_testimonial_section');
/* Function for team section*/
function oceanweb_companion_busicare_testimonial_section()
{
	$home_testimonial_title = get_theme_mod('home_testimonial_title', __('Duis aute irure dolor in reprehenderit', 'oceanweb-companion'));
	$home_testimonial_clientname = get_theme_mod('home_testimonial_name', __('Cras Vitae', 'oceanweb-companion'));	
    $home_testimonial_designation = get_theme_mod('home_testimonial_designation', __('Eu Suscipit', 'oceanweb-companion'));
    $home_testimonial_link = get_theme_mod('home_testimonial_link', __('#', 'oceanweb-companion'));
    $open_new_tab = get_theme_mod('home_testimonial_open_tab',false);        	
    $home_testimonial_thumb = get_theme_mod('home_testimonial_thumb', OCEANWEB_COMPANION_PLUGIN_URL . '/inc/busicare/images/testimonial/testi1.jpg');
    $home_testimonial_desc = get_theme_mod('home_testimonial_desc', __('Sed ut Perspiciatis Unde Omnis Iste Sed ut perspiciatis unde omnis iste natu error sit voluptatem accu tium neque fermentum veposu miten a tempor nise. Duis autem vel eum iriure dolor in hendrerit in vulputate velit consequat reprehender in voluptate velit esse cillum duis dolor fugiat nulla pariatur.', 'oceanweb-companion'));


$testimonial_options = get_theme_mod('busicare_testimonial_content');
$home_testimonial_section_title = get_theme_mod('home_testimonial_section_title', __('Nam Viverra Iaculis Finibus', 'oceanweb-companion'));
$testimonial_callout_background = get_theme_mod('testimonial_callout_background', OCEANWEB_COMPANION_PLUGIN_URL . '/inc/busicare/images/testimonial/testimonial-bg.jpg');
if(get_theme_mod('testimonial_section_enable',true)==true):?>
<section class="section-space testimonial" style="background:url('<?php echo esc_url($testimonial_callout_background); ?>') 112% 50% no-repeat; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover;  background-size: cover;">
    <div class="overlay"></div>
     <div class="container">
       <?php if ($home_testimonial_section_title != '' ) { ?>
       <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="section-header">
                    <h2 class="section-title text-white"><?php echo esc_html($home_testimonial_section_title); ?></h2>
                    <div class="title_seprater"></div>
                </div>
            </div>
        </div>
        <?php } ?>

        <!--Testimonial-->
        <div class="row">
	    <?php $allowed_html = array(
	            'br' => array(),
	            'em' => array(),
	            'strong' => array(),
	            'b' => array(),
	            'i' => array(),
	        );?>
            <div class="col-md-12" >
                <blockquote class="testmonial-block text-center">
                    <?php $default_arg = array('class' => "img-circle"); ?>
                    <?php if ($home_testimonial_thumb != ''): ?>
                        <figure class="avatar">
                            <img src="<?php echo esc_url($home_testimonial_thumb); ?>" class="img-fluid rounded-circle" alt="<?php echo esc_attr($home_testimonial_clientname);?>" >
                        </figure>
                    <?php endif;
					if (!empty($home_testimonial_desc)): ?>
                        <div class="entry-content">
                            <?php if ($home_testimonial_title != ''){ ?><h3 class="title"><span>“ <?php echo wp_kses(html_entity_decode($home_testimonial_title), $allowed_html); ?> ”</span></h3><?php } ?>

                            <?php if ($home_testimonial_desc != '') { ?><p class="text-white" ><?php echo wp_kses(html_entity_decode($home_testimonial_desc), $allowed_html); ?></p> <?php } ?>
                        </div>  
                    <?php endif;
                    if ($home_testimonial_clientname != '' || $home_testimonial_designation != '' ) { ?>                              
                        <figcaption>
                            <?php if (!empty($home_testimonial_designation)): ?>
                            <a href="<?php if (empty($home_testimonial_link)) {echo '#';} else { echo esc_url($home_testimonial_link);}?>" <?php if($open_new_tab==true) { ?> target="_blank"<?php } ?>>
                                    <cite class="name"><?php echo esc_html($home_testimonial_clientname); ?></cite></a>
                            <?php endif; ?>
                            <?php if (!empty($home_testimonial_designation)): ?>
                             <span class="designation"><?php echo esc_html($home_testimonial_designation); ?></span>
                         	<?php endif; ?>
                        </figcaption>
                    <?php } ?>
                </blockquote>
            </div>                          
        </div>
    </div>
</section>
<?php endif;?> 
<!-- /End of Testimonial Section-->
<?php } ?>