<?php
/**
 * Slider section for the homepage.
 */
add_action('oceanweb_companion_busicare_slider_action','oceanweb_companion_busicare_slider_section');

function oceanweb_companion_busicare_slider_section()
{
	$home_slider_subtitle = get_theme_mod('home_slider_subtitle',__('Nulla nec dolor sit amet lacus molestie','oceanweb-companion'));
	$home_slider_image = get_theme_mod('home_slider_image',OCEANWEB_COMPANION_PLUGIN_URL .'inc/busicare/images/slider/slider.jpg');
	$home_slider_title = get_theme_mod('home_slider_title',__('Nulla nec dolor sit amet lacus molestie','oceanweb-companion'));		
	$home_slider_discription = get_theme_mod('home_slider_discription',__('Sea summo mazim ex, ea errem eleifend definitionem vim. Ut nec hinc dolor possim <br> mei ludus  efficiendi ei sea summo mazim ex.','oceanweb-companion'));
	$home_slider_btn_txt = get_theme_mod('home_slider_btn_txt',__('Nec Sem','oceanweb-companion'));
	$home_slider_btn_link = get_theme_mod('home_slider_btn_link',__(esc_url('#'),'oceanweb-companion'));
	$home_slider_btn_target = get_theme_mod('home_slider_btn_target',false);

	$home_slider_btn_txt2 = get_theme_mod('home_slider_btn_txt2',__('Cras Vitae','oceanweb-companion'));
	$home_slider_btn_link2 = get_theme_mod('home_slider_btn_link2',__(esc_url('#'),'oceanweb-companion'));
	$home_slider_btn_target2 = get_theme_mod('home_slider_btn_target2',false);
	
	if(get_theme_mod('home_page_slider_enabled',true)==true) {?>
	<!-- Slider Section -->	
	<section class="bcslider-section">
			<div class="home-section back-img" <?php if($home_slider_image!='') { ?>style="background-image:url( <?php echo esc_url($home_slider_image); ?> );" <?php } ?>>
				<div class="container slider-caption">
					<div class="caption-content text-center">
                        <?php if($home_slider_subtitle!=''){ ?>
                        	<p class="heading"><?php echo esc_html__($home_slider_subtitle); ?></p> 
						<?php }
						if($home_slider_title!=''){ ?>
							<h2 class="title"><?php echo  esc_html__($home_slider_title); ?></h2>
						<?php } 
						if($home_slider_discription!=''){ ?>
							<p class="description"><?php echo  wp_kses_post($home_slider_discription); ?></p>
						<?php } ?>
						<?php if(($home_slider_btn_txt !=null) || ($home_slider_btn_txt2 !=null)) { ?>
						<div class="btn-combo mt-5">
							<?php if($home_slider_btn_txt !=null): ?>
								<a href="<?php echo esc_url($home_slider_btn_link); ?>" <?php if($home_slider_btn_target) { ?> target="_blank" <?php } ?> class="btn-small btn-default"> <?php echo  esc_html__($home_slider_btn_txt); ?> </a>
							<?php endif; ?>											
							<?php if($home_slider_btn_txt2 !=null): ?>
									<a href="<?php echo esc_url($home_slider_btn_link2); ?>" <?php if($home_slider_btn_target2) { ?> target="_blank" <?php } ?> class="btn-small btn-light"><?php echo  esc_html__($home_slider_btn_txt2); ?></a>
							<?php endif;?>
						</div>
						<?php } ?>						
					</div>
				</div>
					<?php $slider_image_overlay = get_theme_mod('slider_image_overlay',true);
						$slider_overlay_section_color = get_theme_mod('slider_overlay_section_color','rgba(0,0,0,0.6)');
					if($slider_image_overlay != false) { ?>
						<div class="overlay" style="background-color:<?php echo esc_attr($slider_overlay_section_color);?>"></div>
					<?php } ?>
    		</div>						
	</section>
	<?php } ?>
	<div class="clearfix"></div>
<?php } ?>