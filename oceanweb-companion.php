<?php
/*
Plugin Name: Oceanweb Companion
Description: Enhances all WordPress Themes functionalities released by OceanwebThemes.
Version: 0.1
Author: OceanwebThemes
Author URI: https://oceanwebthemes.com
Text Domain: oceanweb-companion
*/
define( 'OCEANWEB_COMPANION_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'OCEANWEB_COMPANION_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
function oceanweb_companion_activate() {
	$oceanweb_companion_theme = wp_get_theme(); // gets the current theme
	
    if ( 'BusiCare' == $oceanweb_companion_theme->name || 'BusiCare Child' == $oceanweb_companion_theme->name){
		
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		function oceanweb_companion_busicare_home_page_sanitize_text($input){
			return wp_kses_post( force_balance_tags( $input ) );
		}
		function oceanweb_companion_busicare_sanitize_checkbox($checked) {
	        // Boolean check.
	        return ( ( isset($checked) && true == $checked ) ? true : false );
	    }
		if ( ! is_plugin_active( 'busicare-plus/busicare-plus.php' ) ):

		require_once('inc/controls/customizer-alpha-color-picker/class-oceanweb-companion-customize-alpha-color-control.php');
		require_once('inc/controls/customizer-repeater/functions.php');
			if ( ! function_exists( 'oceanweb_companion_busicare_customize_register' ) ) :
				function oceanweb_companion_busicare_customize_register($wp_customize){
					
					$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
					$sections_customizer_data = array('slider','cta1','services','testimonial','news');	
															
						
					
					if (!empty($sections_customizer_data))
					{ 
						foreach($sections_customizer_data as $section_customizer_data)
						{ 
							require_once('inc/busicare/customizer/'.$section_customizer_data.'-section.php');
						}	
					}
					$wp_customize->remove_control('header_textcolor');
					
				}
			add_action( 'customize_register', 'oceanweb_companion_busicare_customize_register' );
			endif;

			
			    $sections_data = array('slider','cta1','services','testimonial','news');
				
				if (!empty($sections_data))
				{ 
					foreach($sections_data as $section_data)
					{ 
						require_once('inc/busicare/sections/busicare-'.$section_data.'-section.php');
					}	
				}
			
			require_once('inc/busicare/customizer.php');
		
		endif;
	}
}
add_action( 'init', 'oceanweb_companion_activate' );


$oceanweb_companion_theme = wp_get_theme();
//BusiCare
if ( 'BusiCare' == $oceanweb_companion_theme->name || 'BusiCare Child' == $oceanweb_companion_theme->name){		
	register_activation_hook( __FILE__, 'oceanweb_companion_install_function');
	function oceanweb_companion_install_function(){	
		$item_details_page = get_option('item_details_page'); 
	    if(!$item_details_page){
			require_once('inc/busicare/default-pages/upload-media.php');
			require_once('inc/busicare/default-pages/home-page.php');
			require_once('inc/busicare/default-pages/blog-page.php');
			require_once('inc/busicare/default-widgets/default-widget.php');
			update_option( 'item_details_page', 'Done' );
	    }
	}
	function oceanweb_companion_file_replace() {
		    $plugin_dir = plugin_dir_path( __FILE__ ) . 'inc/busicare/wpml-config.xml';
		    $theme_dir = get_stylesheet_directory() . 'wpml-config.xml';
			    if (!copy($plugin_dir, $theme_dir)) {
			        echo "failed to copy $plugin_dir to $theme_dir...\n";
			    }
		}
		add_action( 'wp_head', 'oceanweb_companion_file_replace' );
}

/**
 * Load plugin textdomain.
 */
function oceanweb_companion_load_textdomain() {
  load_plugin_textdomain( 'oceanweb-companion', false, plugin_dir_url(__FILE__). 'languages' ); 

}
add_action( 'init', 'oceanweb_companion_load_textdomain' );